# License
This work is licensed under the terms of the [CC0 1.0 Universal (CC0 1.0) Public Domain Dedication](http://creativecommons.org/publicdomain/zero/1.0/).

![](https://licensebuttons.net/p/zero/1.0/80x15.png)

To the extent possible under law, the person who associated CC0 with this work has waived all copyright and related or neighboring rights to this work.


# Fair Use Policy and Legal Disclaimer
This project contains the images clipped from the manga Family Compo (F.COMPO) the use of which is not specifically authorized by the copyright owner. I am making this project available to the fans community in honor of Hojo Tsukasa. In accord with my non-profit purpose and the following works based on this project, I really hope these efforts can bring me a 'fair use' of containing the copyrighted images of Family Compo (F.COMPO) in this project. If you wish to use the copyrighted images of Family Compo (F.COMPO) for purposes of your own, you must obtain permission from the copyright owner.

  - Based on this project, I am creating my mods about the manga Family Compo (F.COMPO). [Here is that project](https://gitlab.com/family-compo-asset/family-compo-mods).
  - Based on this project, I am writing reviews and analysis & statistics about the manga Family Compo (F.COMPO). [Here is that project](https://gitlab.com/city4cat/myblogs/tree/master/hojocn/).

If containing the copyrighted images of Family Compo (F.COMPO) in this project would not be allowed, please let me know by leaving a message [here](https://gitlab.com/family-compo-asset/family-compo-asset-vol01/issues). 


# 使用许可
本作品采用[CC0 1.0 通用 (CC0 1.0) 公共领域贡献](https://creativecommons.org/publicdomain/zero/1.0/deed.zh)进行许可。

![](https://licensebuttons.net/p/zero/1.0/80x15.png)

在法律允许的范围内，将CC0与该作品相关联的人放弃了对该作品的所有版权以及相关或邻近的权利。


# 合理使用和免责声明
该项目包含了漫画《非常家庭》(F.COMPO)的图片，其使用为得到版权方的明确授权。我把该项目贡献给漫迷是为了向北条司大师致敬。考虑到我的非营利意图和基于该项目所作的其他工作，我真心希望这些努力能让我‘合理地’在本项目里包含《非常家庭》(F.COMPO)的图片。如果你希望使用《非常家庭》(F.COMPO)的图片，请务必获得版权方的许可。

  - 基于本项目, 我正在制作《非常家庭》(F.COMPO)的MOD。[项目在这里](https://gitlab.com/family-compo-asset/family-compo-mods)。
  - 基于本项目, 我正在写《非常家庭》(F.COMPO)评论，以及分析和统计。[项目在这里](https://gitlab.com/city4cat/myblogs/tree/master/hojocn/)。

如果在本项目里包含《非常家庭》(F.COMPO)的图片是不允许的, 请在[这里](https://gitlab.com/family-compo-asset/family-compo-asset-vol01/issues)留言告诉我。

